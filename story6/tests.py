from django.test import TestCase, Client
from django.urls import reverse, resolve
from .models import Activity, Member

class TestViews(TestCase):
    def setUp(self):
        self.client = Client()
        self.activity_url = reverse("story6:activity")
        self.test_activity = Activity.objects.create(
            activity = "makan"
        )
        self.test_member = Member.objects.create(
            member = "rafli",
            aktivitas = self.test_activity
        )
        self.test_delete_url = reverse("story6:activityDelete",args=[self.test_activity.id])
        self.test_member_delete_url = reverse("story6:memberDelete",args=[self.test_member.id])

    def test_activity_GET(self):
        response = self.client.get(self.activity_url)
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, "story6/index.html")

    def test_activity_POST(self):
        response = self.client.post(self.activity_url, {
            "activity" : "tidur"
        }, follow=True)
        self.assertContains(response, "tidur")

    def test_member_POST(self):
        response = self.client.post(self.activity_url, {
            "member" : "okto",
            "aktivitas" : self.test_activity.id,
        }, follow=True)
        self.assertContains(response, "okto")

    def test_notValid_POST(self):
        response = self.client.post(self.activity_url, {
            "member" : "okto",
            "aktivitas" : "haha",
        }, follow=True)
        self.assertContains(response, "Input tidak sesuai")

    def test_member_DELETE(self):
        response = self.client.get(self.test_member_delete_url, follow=True)
        # print(response.content)
        self.assertContains(response, "berhasil dihapus")

    def test_activity_DELETE(self):
        response = self.client.get(self.test_delete_url, follow=True)
        self.assertContains(response, "berhasil dihapus")
